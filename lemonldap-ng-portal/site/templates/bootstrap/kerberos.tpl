<TMPL_IF NAME="CHOICE_PARAM">
 <!-- //if:jsminified
 <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/kerberosChoice.min.js"></script>
 //else -->
 <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/kerberosChoice.js"></script>
 <!-- //endif -->
<TMPL_ELSE>
 <!-- //if:jsminified
 <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/kerberos.min.js"></script>
 //else -->
 <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/kerberos.js"></script>
 <!-- //endif -->
</TMPL_IF>
<div class="form">
  <input type="hidden" name="kerberos" value="0" />
  <div class="sslclick">
    <img src="<TMPL_VAR NAME="STATIC_PREFIX">common/modules/Kerberos.png" alt="<TMPL_VAR NAME="module">" class="img-thumbnail mb-3" />
  </div>

  <TMPL_INCLUDE NAME="impersonation.tpl">
  <TMPL_INCLUDE NAME="checklogins.tpl">

  <button type="submit" class="btn btn-success sslclick" >
    <span class="fa fa-sign-in"></span>
    <span trspan="connect">Connect</span>
  </button>
</div>
